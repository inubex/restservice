package com.kapstone.code.challenge.exceptions;

public class UserAlreadyExistsException extends RuntimeException {

	public UserAlreadyExistsException(String name, String email) {
		super(String.format("User with %s and %s is already present ", name,email));
	}
}
