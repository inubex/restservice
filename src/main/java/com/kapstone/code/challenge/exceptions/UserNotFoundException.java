package com.kapstone.code.challenge.exceptions;

public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(Integer id) {
		super("Could not find user " + id);
	}

	public UserNotFoundException(String field) {
		super(String.format("User with %s is not present ", field));
	}

	public UserNotFoundException(String name, String email) {
		super(String.format("User with %s and %s is not present ", name, email));
	}

	public UserNotFoundException(boolean active) {
		super(String.format("Could not find active %s user ", active));
	}
}
