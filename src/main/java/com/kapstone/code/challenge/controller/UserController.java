package com.kapstone.code.challenge.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kapstone.code.challenge.entities.User;
import com.kapstone.code.challenge.services.UserService;


//H. Endpoint should be started by /api/v1/users.
@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	UserService userService;

	//START:F. If Name, Email(equalsIgnoreCase) exists then prevent the entry.
	@PostMapping("/users")
	public ResponseEntity<User> newUser(@RequestBody @Valid User newUser) {
		return new ResponseEntity<User>(userService.save(newUser),HttpStatus.CREATED);
	}
	//END:F. If Name, Email(equalsIgnoreCase) exists then prevent the entry.
	

	//I. Read, Read All endpoints - read the database records using any crieteria like UserID, Name(equals,equalsIgnoreCase), Email(equals,equalsIgnoreCase),Active(true,false) as given in below points
	//START:	J. To read all users use enpoint /users and /users?Active=true/false - read all User
	@GetMapping("/users")
	@ResponseBody
	public Iterable<User> findAll() {
		return userService.findAll();
	}

	@RequestMapping(value = "/users", params = "active", method = RequestMethod.GET)
	@ResponseBody
	public Iterable<User> findAllWithActive(@RequestParam(name = "active", required = false) boolean active) {
		return userService.findAllWithActive(active);
	}
	//END:	J. To read all users use enpoint /users and /users?Active=true/false - read all User

	//START:	K. To read specific user use enpoint /users/{userId}, /users?name="TestUser", /users?email="TestUser@xyz.com"
	@GetMapping("/users/{id}")
	public User findById(@PathVariable Integer id) {
		return userService.findById(id);
	}

	@RequestMapping(value = "/users", params = "name", method = RequestMethod.GET)
	public User findByNameIgnoreCase(@RequestParam(name = "name", required = false) String name) {
		return userService.findByNameIgnoreCase(name);
	}

	@RequestMapping(value = "/users", params = "email", method = RequestMethod.GET)
	public User findByNameEmailIgnoreCase(@RequestParam(name = "email", required = false) String email) {
		return userService.findByEmailIgnoreCase(email);
	}
	//END: K. To read specific user use enpoint /users/{userId}, /users?name="TestUser", /users?email="TestUser@xyz.com"
	
	//START:L. To delete specific user use enpoint  /users/{userId} or /users?name="TestUser", /users?email="TestUser@xyz.com"
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable Integer id) {
		userService.deleteById(id);
	}
	
	@RequestMapping(value = "/users", params = "name", method = RequestMethod.DELETE)
	public User deleteByNameIgnoreCase(@RequestParam(name = "name", required = false) String name) {
		return userService.deleteByNameIgnoreCase(name);
	}

	@RequestMapping(value = "/users", params = "email", method = RequestMethod.DELETE)
	public User deleteByNameEmailIgnoreCase(@RequestParam(name = "email", required = false) String email) {
		return userService.deleteByEmailIgnoreCase(email);
	}
	//END:L. To delete specific user use enpoint  /users/{userId} or /users?name="TestUser", /users?email="TestUser@xyz.com"

	//START: M. To delete bulk users use enpoint /users,/users?active=true/false 
	@RequestMapping(value = "/users", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteAll() {
		userService.deleteAll();
	}
	
	@RequestMapping(value = "/users", params = "active", method = RequestMethod.DELETE)
	@ResponseBody
	public List<User> deleteByActive(@RequestParam(name = "active", required = false) boolean active) {
		return userService.deleteByActive(active);
	}
	//END: M. To delete bulk users use enpoint /users,/users?active=true/false
	
	//START:	O. To update specific user use enpoint /users/{userId},/users?name="TestUser", /users?email="TestUser@xyz.com"
	@PutMapping("/users/{id}")
	public User updateUser(@RequestBody @Valid User updatedUser, @PathVariable Integer id) {
		
		return userService.updateUser(updatedUser, id);
	}
	
	@RequestMapping(value = "/users", params = "name", method = RequestMethod.PUT)
	public User updateUserWithName(@RequestParam(name = "name", required = false) String name,@RequestBody @Valid User updatedUser) {
		
		return userService.updateUserWithName(name,updatedUser);
	}

	@RequestMapping(value = "/users", params = "email", method = RequestMethod.PUT)
	public User updateUserWithEMail(@RequestParam(name = "email", required = false) String email, @RequestBody @Valid User updatedUser) {
		
		return userService.updateUserWithEMail(email,updatedUser);
	}
	//END:	O. To update specific user use enpoint /users/{userId},/users?name="TestUser", /users?email="TestUser@xyz.com"
	
	
}
