package com.kapstone.code.challenge;

import java.util.Base64;
import java.util.Base64.Encoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CodeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeChallengeApplication.class, args);
	}
	
	
	//START:	C. Password should be in Base64 encryption format.
	@Bean
	public Encoder encodePassword()
	{
		return Base64.getEncoder();
		
	}
	//END :	C. Password should be in Base64 encryption format.

}
