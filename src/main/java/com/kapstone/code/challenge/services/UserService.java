package com.kapstone.code.challenge.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kapstone.code.challenge.entities.User;
import com.kapstone.code.challenge.exceptions.UserNotFoundException;
import com.kapstone.code.challenge.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	//C. Password should be in Base64 encryption format.
	@Autowired
	Encoder encodePassword;

	public Iterable<User> findAll() {
		return userRepository.findAll();
	}

	public List<User> findAllList() {
		return userRepository.findAll();
	}

	public User save(User newUser) {
		
		newUser.setCreateDate(LocalDateTime.now());
		isBase64(newUser);

		boolean exists = userRepository.existsByNameIgnoreCaseOrEmailIgnoreCase(newUser.getName(), newUser.getEmail());

		if (!exists) {
			return userRepository.save(newUser);
		} else {
			throw new UserNotFoundException(newUser.getName(), newUser.getEmail());
		}
	}

	public User findById(Integer id) {
		return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}

	public User updateUser(User newUser, Integer id) {
		return userRepository.findById(id).map(user -> {
			user.setUpdatedDate(LocalDateTime.now());
			return userRepository.save(user);
		}).orElseGet(() -> {
			newUser.setCreateDate(LocalDateTime.now());
			return userRepository.save(newUser);
		});
	}

	public void deleteById(Integer id) {
		userRepository.deleteById(id);
	}

	public Iterable<User> findAllWithActive(boolean active) {

		return userRepository.findByActive(active).orElseThrow(() -> new UserNotFoundException(active));
	}

	public User findByNameIgnoreCase(String name) {
		return userRepository.findByNameIgnoreCase(name)
				.orElseThrow(() -> new UserNotFoundException(name));
	}
	
	public User findByEmailIgnoreCase(String email) {
		return userRepository.findByEmailIgnoreCase(email)
				.orElseThrow(() -> new UserNotFoundException(email));
	}
	

	@Transactional
	public  List<User> deleteByActive(boolean active) {
		return userRepository.deleteByActive(active).orElseThrow(() -> new UserNotFoundException(active));
	}
	@Transactional
	public User deleteByNameIgnoreCase(String name) {
		return userRepository.deleteByNameIgnoreCase(name).orElseThrow(() -> new UserNotFoundException(name));
	}
	@Transactional
	public User deleteByEmailIgnoreCase(String email) {
		return userRepository.deleteByEmailIgnoreCase(email).orElseThrow(() -> new UserNotFoundException(email));
	}
	@Transactional
	public void deleteAll() {
		userRepository.deleteAll();
	}
	
	@Transactional
	public User updateUserWithName(String name,User updatedUser) {
		
		isBase64(updatedUser);
		
		return userRepository.findByEmailIgnoreCase(name).map(user -> {
			user.setUpdatedDate(LocalDateTime.now());
			return userRepository.save(user);
		}).orElseGet(() -> {
			updatedUser.setCreateDate(LocalDateTime.now());
			return userRepository.save(updatedUser);
		});
	}
	
	@Transactional
	public User updateUserWithEMail(String email,User updatedUser) {
		
		isBase64(updatedUser);
		
		return userRepository.findByEmailIgnoreCase(email).map(user -> {
			user.setUpdatedDate(LocalDateTime.now());
			return userRepository.save(user);
		}).orElseGet(() -> {
			updatedUser.setCreateDate(LocalDateTime.now());
			return userRepository.save(updatedUser);
		});
	}
	
	public void isBase64(User updatedUser)
	{
		if(!org.apache.commons.codec.binary.Base64.isBase64(updatedUser.getPassword().getBytes()))
		{
			updatedUser.setPassword(encodePassword.encodeToString(updatedUser.getPassword().getBytes()));
		}
	}

}
