package com.kapstone.code.challenge.advice;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kapstone.code.challenge.exceptions.UserAlreadyExistsException;
import com.kapstone.code.challenge.exceptions.UserNotFoundException;

@ControllerAdvice
class UserControllerAdvice {

  @ResponseBody
  @ExceptionHandler(UserNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String userNotFoundHandler(UserNotFoundException ex) {
    return ex.getMessage();
  }
  
  @ResponseBody
  @ExceptionHandler(UserAlreadyExistsException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public String userAlreadyExistsHandler(UserAlreadyExistsException ex) {
    return ex.getMessage();
  }
  
  @ResponseBody
  @ExceptionHandler(EmptyResultDataAccessException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String emptyResultDataAccessException(EmptyResultDataAccessException ex) {
    return "Data not found!";
  }
  
  @ResponseBody
  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public String emptyResultDataAccessException(Exception ex) {
    return "Please contact to administrator!";
  }
  
  
}