package com.kapstone.code.challenge.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kapstone.code.challenge.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	boolean existsByNameIgnoreCaseOrEmailIgnoreCase(String name, String email);

	Optional<Iterable<User>> findByActive(boolean active);

	Optional<User> findByNameIgnoreCase(String name);
	
	Optional<User> findByEmailIgnoreCase(String email);
	

//	Long deleteByActive(boolean active);

	Optional<List<User>> deleteByActive(boolean active);

	Optional<User> deleteByNameIgnoreCase(String name);

	Optional<User> deleteByEmailIgnoreCase(String email);
}
