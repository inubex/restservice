package com.kapstone.code.challenge.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class User {
	
	@Override
	public String toString() {
		return "User [userID=" + userID + ", name=" + name + ", password=" + password + ", email=" + email
				+ ", createDate=" + createDate + ", updatedDate=" + updatedDate + ", active=" + active + "]";
	}
	
	//	A. While inserting the user, user-id should be unique and auto-increment.
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userID;
	
	//START:	B. Name, Password, Email should not be blank/null/empty.
	@NotEmpty(message = "Name is mandatory")
	@NotBlank(message = "Name is mandatory")
	@NotNull(message = "Name cannot be null")
	private String name;
	
	@NotEmpty(message = "Password is mandatory")
	@NotBlank(message = "Password is mandatory")
	@NotNull(message = "Password cannot be null")
	private String password;
	

	//	D. Email should be validated with email format.	
	@NotEmpty(message = "Email is mandatory")
	@NotBlank(message = "Email is mandatory")
	@NotNull(message = "Email cannot be null")
	@Size(
			  min = 5,
			  max = 50,
			  message = "The user email '${validatedValue}' must be between {min} and {max} characters long"
			)
	@Email(message = "Email should be valid")
	private String email;
	
	//START: E. CreateDate and UpdatedDate in a Database table with dd-MM-yyyy HH:mm:ss
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime createDate;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private LocalDateTime updatedDate;
	//END: E. CreateDate and UpdatedDate in a Database table with dd-MM-yyyy HH:mm:ss
	
	//	G. Active column value should be true/false. when creating a new user that time Active value should be true. 
	private boolean active=true;
		
	public User() {
		super();
	}
	
	public User(int userID, String name, String password, String email, LocalDateTime createDate,
			LocalDateTime updatedDate, boolean active) {
		super();
		this.userID = userID;
		this.name = name;
		this.password = password;
		this.email = email;
		this.createDate = createDate;
		this.updatedDate = updatedDate;
		this.active = active;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	
	

}
